# BELT and phenoSEED
This repository holds code developed for a system of image acquisition (BELT) and an accompanying processing 
system(phenoSEED. BELT is a conveyor based system used to capture high resolution images of seeds. The system is 
detailed in the publication "The BELT and phenoSEED platforms: shape and colour phenotyping of seed samples" which is 
available [in Plant Methods.](https://plantmethods.biomedcentral.com/articles/10.1186/s13007-020-00591-8)
Some items have been updated since the publishing of that paper, including a re-write of processing and porting 
acquisition from LabView to Python. Settings important to both BELT and phenoSEED are contained in camsettings.yaml.

Please contact the main author of the paper with any questions about the BELT system or phenoSEED.
## BELT
BELT.py is the main image acquisition script that is designed to acquire images in batch runs. A PySimpleGUI window 
displays images from all attached genicam cameras and some simple options. Once in acquisition mode, the cameras are set
to trigger mode and are triggered by an arduino flashed with BELT_arduino that monitors infrared emitter-detector pairs.
Acquired images are saved as 16 bit integers as numpy binary files (.npy) while smaller thumbnails (20% of the original 
size) are saved as .png images.

## phenoSEED

phenoSEED is the main processing script. .npy (or .npz) files are loaded in, colour corrected with a trained ANN (see 
[calibration/colourCalibration](calibration/colourCalibration.py)), cropped and segmented to extract the front view and
side view of the seeds, and fed into several functions to extract information. Current functions extract morphological 
features, statistics of colour distribution across the seed coat, and attempt to cluster the colours in to two groups. 
These were initially created to analyze lentils where roundness and sphericity are important growth markers and the seed
coat can sometimes be split into a base and pattern colour. 

.npz files of colour corrected images and associated masks are saved along with a csv of results. When phenoSEED is 
supplied with .npz files, computationally expensive steps of colour correction are skipped. If users want to rerun 
with different processing, overall speed will be greatly increased. 

### Calibration
Images for calibration can easily be captured with [singleCapture](calibration/singleCapture.py). Two feeds are shown, 
the full size image which will be captured and saved, as well as a cropped image indicative of what will be fed to 
[colourCalibration](calibration/colourCalibration.py). Make sure the cropped image is free of debris and the surface of 
your reference is not damaged.
#### Colour
Colour calibration is achieved by fitting a multi-layer perceptron for regression. Our work uses an 
[X-Rite ColorChecker Digital SG](https://www.xrite.com/categories/calibration-profiling/colorchecker-digital-sg), but be
 aware that the colorimetric values supplied by X-Rite can be used for personal or educational purposes but a commercial
 usage [requires a license](https://www.xrite.com/service-support/colorimetric_values_for_colorchecker_digital_sg). 
 If you are within your rights to use the calibration data, save 
 [the .txt file of the calibration table](https://my.xrite.com/documents/apps/public/digital_colorchecker_sg_l_a_b.txt) 
 in the [calibration](calibration) folder.
 
 [The colour calibration function](calibration/colourCalibration.py) will print some stats after fitting the MLPR  and 
 output a csv. Some handpicked information from the CSV can be shown visually by calling 
 [generateFiguresFromReport.](calibration/generateFiguresFromReport.py)
#### Height
This procedure is less developed. Image a precise measurement tool (such as the ruler from the X-Rite color checker) in
three ways with the tick marks horizontal in the image: facing the camera, facing the mirrored prism at near distance 
and facing the mirrored prism at a far distance. Crop these images to only the ruler and use a colourspace transform 
then sum the rows of the image after applying sobel horizontal filter to locate the edges. Use the scipy find_peaks 
function to locate the pixel distances of the peaks and linearly fit to the tick deviations. For the side height 
scaling, record the column position of the ruler centre and linearly interpolate your scaling values between 'far' 
and 'near'. All of these values can be set in camsettings.yaml under 'processing' for each camera.

    from skimage.filters import sobel_h
    from skimage.color import rgb2hsv
    from scipy.signal import find_peaks
    from scipy.stats import linregress
    peaky_values = sobel_h(rgb2hsv(cropped_image)[...,2]).sum(axis=1)
    pixel_distances = find_peaks(peaky_values, height=3, distance=20)  # adjust parameters if necessary
    fit = linregress(pixel_distances, {physical_distances})  # physical distances are likely [0,1,2,...] [mm]

# Usage

The python script can be invoked from a command line with one required argument: 
*  -i --input_path path to the top level directory which will be searched recursively for .npz files and then .npy files

Optional arguments include: 
*  -s --settings a path to the camsettings.yaml file if it is not in the same directory
*  -o --output_path a folder to save the results to
