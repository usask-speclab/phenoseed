#  File: BELT.py
#  Description: Main level file for BELT acquisition. spawns the main screen and hooks into cameras
#
#  Author: Keith Halcro
#  Copyright: University of Saskatchewan, 2020
#

import os
import sys
import argparse
import logging
from multiprocessing import Queue, Process

import serial
import serial.tools.list_ports
import cv2
import numpy as np
import yaml
import PySimpleGUI as sg

from harvesters.core import Harvester
from genicam.gentl import ResourceInUseException, TimeoutException


class BeltCamera:
    """
    simple class to associate a harvesters image_acquirer object with queues
    """
    def __init__(self, image_acquirer, config_dict):
        """
        :param image_acquirer: a harvesters image acquirer object
        :param: config_dict: the dictionary of camera settings keyed by serial number
        """
        self.ia = image_acquirer
        self.sn = self.ia.device.node_map.DeviceSerialNumber.value
        self.dq = Queue()  # dataqueue from device
        self.sq = Queue()  # savequeue to run save to disk in background
        self.mq = Queue()  # messagequeue to turn acquisition on and off
        self.count = 0
        if self.sn not in config_dict.keys():
            raise('Device S/N {} is not found in camsettings.yaml'.format(self.sn))
        else:
            self.config = config_dict[self.sn]['acquisition']
            self.camera_setup()
            self.capture_enable = False
            self.ia.start_acquisition()

    def camera_setup(self):
        """
        sets up camera parameters, mirrors labview program CameraSetup_SubVI.vi
        :return: none, adjusts camera params
        """
        nodemap = self.ia.remote_device.node_map
        nodemap.PixelFormat.value = 'BayerRG16'
        nodemap.TriggerSource.value = 'Line2'
        nodemap.TriggerActivation.value = 'RisingEdge'
        nodemap.TriggerDelayEnabled.value = True
        nodemap.TriggerMode.value = 'On'
        nodemap.TriggerOverlap.value = 'ReadOut'
        nodemap.ExposureMode.value = 'Timed'
        nodemap.ExposureAuto.value = 'Off'
        nodemap.GainAuto.value = 'Off'
        nodemap.BalanceWhiteAuto.value = 'Off'
        nodemap.pgrExposureCompensationAuto.value = 'Off'
        nodemap.AcquisitionMode.value = 'Continuous'

        nodemap.TriggerDelay.value = self.config['trigger_delay']
        nodemap.Gain.value = self.config['gain']
        nodemap.ExposureTime.value = self.config['exposure_time']

        nodemap.BalanceRatioSelector.value = 'Blue'
        nodemap.BalanceRatio.value = self.config['balance_ratio_blue']
        nodemap.BalanceRatioSelector.value = 'Red'
        nodemap.BalanceRatio.value = self.config['balance_ratio_red']

        nodemap.Width.value = self.config['width']
        nodemap.Height.value = self.config['height']
        nodemap.OffsetX.value = self.config['offset_x']
        nodemap.OffsetY.value = self.config['offset_y']

    def manual_capture(self):
        """
        gets a single image, independent of queues and triggers.
        :return:
        """
        self.ia.remote_device.node_map.TriggerSource.value = 'Software'
        self.ia.remote_device.node_map.TriggerSoftware.execute()
        try:
            with self.ia.fetch_buffer() as buf:
                comp = buf.payload.components[0]
                img = np.copy(comp.data.reshape(comp.height, comp.width))
            return img
        except TimeoutException:
            return None
        finally:
            self.ia.remote_device.node_map.TriggerSource.value = 'Line2'

    def count_reset(self):
        """
        reset internal count to delineate acquisition events
        :return:
        """
        self.count = 0

    def capture(self):
        """
        turns on the camera with triggermode on and begins pushing images acquired when triggered to the dataqueue
        :return:
        """
        if not self.mq.empty():
            new_mq_read = self.mq.get()
            if new_mq_read == self.capture_enable:
                pass
            else:
                self.capture_enable = new_mq_read
                if self.capture_enable:
                    try:
                        self.ia.start_acquisition()
                    except ResourceInUseException:
                        pass
                elif not self.capture_enable:
                    self.ia.stop_acquisition()
        if self.capture_enable:
            try:
                # logging.debug('attempting to get buffer from {}'.format(self.sn))
                with self.ia.fetch_buffer(timeout=0.1) as buf:
                    comp = buf.payload.components[0]
                    img = np.copy(comp.data.reshape(comp.height, comp.width))
                    self.dq.put([img, self.count])
                    self.count += 1
                    logging.debug('image captured for {} and pushed to queue'.format(self.sn))
            except TimeoutException:
                pass

    def count_saved_files(self, save_dir, sample):
        """
        helper function to find number of saved files for given sample in a given directory
        :param save_dir: top level save directory
        :param sample: directory to search for this camera's images
        :return: a count of images saved by this object for current run
        """
        try:
            return len([name for name in os.listdir(os.path.join(save_dir, sample))
                        if os.path.isfile(os.path.join(save_dir, sample, name)) and self.sn in name and save_dir != ''])
        except FileNotFoundError:
            return 0


def save_images(camera, enable_queue, make_thumbnails=True):
    """
    process that waits for items in the queue then saves them supposed to be multiprocessed
    :param camera: a single camera object
    :param enable_queue: a queue that when False is put into, will stop the process
    :param make_thumbnails: boolean flag to also save small png images in a directory 'thumbnails'
    :return: nothing. saves images to disk
    """
    while True:
        if not camera.sq.empty():
            base_dir, sample, img, count = camera.sq.get()
            save_dir = os.path.join(base_dir, sample)
            img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2RGB_EA)  # to RGB for numpy
            os.makedirs(save_dir, exist_ok=True)
            # have to use count passed by queue since camera.count will not update when incremented in the main process
            np.save(os.path.join(save_dir, '{}_{}.npy'.format(camera.sn, count)), img)
            logging.info('{} saved'.format(os.path.join(save_dir, '{}_{}'.format(camera.sn, count))))
            if make_thumbnails:
                # debayer, resize, gamma correction then write
                thumbnail = cv2.resize(cv2.cvtColor(img, cv2.COLOR_RGB2BGR), dsize=(0, 0), fx=0.2, fy=0.2)
                thumbnail = (np.power(thumbnail / np.iinfo(thumbnail.dtype).max, 1/2.2) * 255).astype(np.uint8)
                os.makedirs(os.path.join(save_dir, 'thumbnails'), exist_ok=True)
                cv2.imwrite(os.path.join(save_dir, 'thumbnails', '{}_{}.jpg'.format(camera.sn, count)), thumbnail)
        if not enable_queue.empty():
            if not enable_queue.get():  # if False is in the queue
                break


def operator_screen(cameras, arduino):
    """
    puts up the main screen
    :param cameras: BeltCamera objects
    :param arduino: a pyserial object to connect to the arduino
    :return:
    """
    # the layout (image sizes, font sizes) may have to adjusted - this works with two cameras on a small monitor
    sg.theme('DefaultNoMoreNagging')
    layout = [[sg.Text('Save Location - Include Username', font='Arial 16'), sg.InputText(f'/shared/DATA/{os.getlogin()}',
                                                                                          key='-savedir-',
                                                                                          font='Arial 16'),
               sg.FolderBrowse(target='-savedir-', font='Arial 16'), sg.Button('E-STOP  OFF', key='-E-STOP-')],
              [sg.Button('Start Acquisition', size=(20, 3), font='Arial 16', button_color=(None, 'yellow')),
               sg.Button('Next Sample', size=(20, 3), font='Arial 16', button_color=(None, 'grey')),
               sg.Button('Stop Acquisition', size=(20, 3), font='Arial 16', button_color=(None, 'red'))],
              [sg.Text('Sample Number', font='Arial12'), sg.InputText(key='-input-', font='Arial12')],
              [sg.Frame('Camera: {}'.format(i.sn), title_color='black',
                        layout=[[sg.Image(filename='', key='-cam{}-'.format(i.sn), size=(375, 550))],
                                [sg.Text('Acquired: '), sg.Text(' '*15, key='-numacq{}-'.format(i.sn)),
                                 sg.Text('Saved: '), sg.Text(' '*15, key='-numsav{}-'.format(i.sn))]])
               for i in cameras],
              [sg.Exit(size=(30, 4), font='Arial15')]]
    window = sg.Window('BELT Image Acquisition', layout, element_justification='center', resizable=True).Finalize()

    # TODO: is this initialization required?
    for cam in cameras:
        init_img = cv2.resize(cv2.cvtColor(cam.manual_capture(), cv2.COLOR_BAYER_BG2BGR_EA), dsize=(0, 0), fx=0.2, fy=0.2)
        init_img = (np.power(init_img / np.iinfo(init_img.dtype).max, 1/2.2) * 255).astype(np.uint8)
        init_img_bytes = cv2.imencode('.png', init_img)[1].tobytes()
        window['-cam{}-'.format(cam.sn)].update(data=init_img_bytes)

    while True:
        # no waiting so the window can stream images by requesting the image instead of updating on user interaction
        event, values = window.read(timeout=20, timeout_key='-timeout-')
        # if window closes, break this loop
        if event in (None, 'Exit'):
            break

        if not os.path.isdir(values['-savedir-']):  # catch errors when the user doesn't specify a user
            sg.Popup('Please select your user folder e.g. "/shared/DATA/{NSID}"')
            continue

        # disable saving, disable all arduino controlled processes, and delete the sample name
        if event == 'Stop Acquisition':
            logging.info('Stop Acquisition Button Press')
            for cam in cameras:
                cam.mq.put(False)
            if 'sample' in locals():
                del sample
            arduino.write(b'\n'.join('{}_DISABLE'.format(value).encode() for value in ['P', 'C', 'M']))

        # lock in the sample name, start the save processes and enable all control lines on the arduino
        if event == 'Start Acquisition':
            logging.info('Start Acquisition Button Press')
            window['-input-'].update(disabled=True)
            sample = values['-input-']
            save_dir = os.path.join(values['-savedir-'], sample)
            if os.path.isdir(save_dir):  # already exists so it must be a repeat
                window['-input-'].update('Duplicate Sample Name', background_color='red', disabled=False, select=True)
                continue
            else:
                window['-input-'].update(background_color='white')
            for cam in cameras:
                cam.mq.put(True)
            arduino.write(b'\n'.join('{}_ENABLE'.format(value).encode() for value in ['P', 'C', 'M']))

        # same as stop, but immediately draw the user to update the sample
        if event == 'Next Sample':
            logging.info('Next Sample Button Press')
            window['-input-'].update(disabled=False, select=True)  # ready to accept
            [cam.count_reset() for cam in cameras]  # start counting at 0 for this sample
            for cam in cameras:
                cam.mq.put(False)
            arduino.write(b'\n'.join('{}_DISABLE'.format(value).encode() for value in ['P', 'C', 'M']))
            if 'sample' in locals():  # avoid errors
                del sample

        # read the arduino
        msg = arduino.readline()
        if msg != b'':  # msg = b'' if nothing returned due to timeout
            estop_status = True if msg == b'ESTOP STATUS: 1\r\n' else False
            window['-E-STOP-'].update('E-STOP  {}'.format(('OFF', 'ON')[estop_status]),
                                      button_color=('white', ('green', 'red')[estop_status]))

        # push count of acquired images to window
        [window['-numacq{}-'.format(cam.sn)].update(cam.count) for cam in cameras]

        if 'sample' in locals():  # each iteration, call capture if there is a sample available
            for cam in cameras:
                window['-numsav{}-'.format(cam.sn)].update(cam.count_saved_files(values['-savedir-'], sample))
                cam.capture()
                if not cam.dq.empty():
                    # logging.debug('received image on the dq for {}'.format(cam.sn))
                    img, count = cam.dq.get()
                    cam.sq.put([values['-savedir-'], sample, img, count])
                    img = cv2.resize(cv2.cvtColor(img, cv2.COLOR_BAYER_BG2BGR_EA), dsize=(0, 0), fx=0.2, fy=0.2)  # to BGR for opencv
                    img = (np.power(img / np.iinfo(img.dtype).max, 1/2.2) * 255).astype(np.uint8)
                    img_bytes = cv2.imencode('.png', img)[1].tobytes()
                    window['-cam{}-'.format(cam.sn)].update(data=img_bytes)
        else:  # fallback - update cameras if no acquisition is happening
            for cam in cameras:
                img = cam.manual_capture()
                img = cv2.resize(cv2.cvtColor(img, cv2.COLOR_BAYER_BG2BGR_EA), dsize=(0, 0), fx=0.2, fy=0.2)
                img = (np.power(img / np.iinfo(img.dtype).max, 1/2.2) * 255).astype(np.uint8)
                init_img_bytes = cv2.imencode('.png', img)[1].tobytes()
                window['-cam{}-'.format(cam.sn)].update(data=init_img_bytes)


def belt_main_level(settings_path=None, arduino_port=None):
    """
    BELT main level - starts all of the devices and then shows the acquisition screen

    :return: nothing
    """
    # harvesters setup
    h = Harvester()
    h.add_cti_file('/opt/mvIMPACT_Acquire/lib/x86_64/mvGenTLProducer.cti')
    h.update_device_info_list()

    # connect to arduino
    try:
        if arduino_port is None:
            ports = list(serial.tools.list_ports.comports())
            selected_port = [p for p in ports if 'ACM' in p.description][0]
            arduino = serial.Serial(selected_port.device, 9600, timeout=0)
        else:
            arduino = serial.Serial(arduino_port, 9600, timeout=0)
    except serial.serialutil.SerialException as e:
        raise e
    try:
        if settings_path is not None:
            with open(settings_path, 'r') as f:
                config_dict = yaml.safe_load(f)
        else:
            with open(os.path.join(sys.path[0], 'camsettings.yaml'), 'r') as f:
                config_dict = yaml.safe_load(f)
    except FileNotFoundError:
        raise(f'camsettings.yaml not found at '
              f'{os.path.join(sys.path[0], "camsettings.yaml") if settings_path is None else settings_path}')

    # create harvester image acquirers and put then in BeltCamera class and establish communication
    image_acquirers = [h.create_image_acquirer(serial_number=config_sn) for config_sn in sorted(config_dict.keys())]
    cameras = [BeltCamera(ia, config_dict) for ia in image_acquirers]
    save_enable_queues = [Queue() for ia in image_acquirers]
    save_processes = [Process(target=save_images, args=(cam, sq)) for cam, sq in zip(cameras, save_enable_queues)]
    [p.start() for p in save_processes]

    # start the main screen
    operator_screen(cameras, arduino)

    # cleanup after screen is closed
    if sum([cam.sq.empty() for cam in cameras]) == len(cameras):  # check if all savequeues are empty
        [cam.ia.stop_acquisition() for cam in cameras]
        [cam.ia.destroy() for cam in cameras]
        h.reset()
        [sq.put(False) for sq in save_enable_queues]
        [p.join() for p in save_processes]
        arduino.write(b'\n'.join('{}_DISABLE'.format(value).encode() for value in ['P', 'C', 'M']))
        arduino.close()


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-s', '--settings', required=False, type=str, help='path to the camsettings.yaml file - defaults '
                                                                       'to same directory as this script')
    ap.add_argument('-a', '--arduino', required=False, type=str, help='path to port for the arduino - defaults to '
                                                                      'searching ports that contain "ACM"')
    ap.add_argument("--log", help="The desired log level to be displayed", default="INFO",
                    choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
    args = ap.parse_args()
    log_level = getattr(logging, args.log.upper(), getattr(logging, 'INFO'))
    logging.basicConfig(level=log_level)
    belt_main_level(args.settings, args.arduino)
