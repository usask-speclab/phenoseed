#  File: singleCapture.py
#  Description: Main level file for BELT acquisition. spawns the main screen and hooks into cameras
#
#  Author: Keith Halcro
#  Copyright: University of Saskatchewan, 2020
#

import PySimpleGUI as sg
from harvesters.core import Harvester
import numpy as np
import cv2
import yaml

import os


def configure_device(image_acquirer):
    """
    applies a bunch of settings to the camera device
    :param image_acquirer: a harvesters image acquirer object
    :return: the modified image acquirer
    """
    nodemap = image_acquirer.remote_device.node_map
    with open('./camsettings.yaml', 'r') as f:
        config_dict = yaml.safe_load(f)
    config = config_dict[image_acquirer.device.node_map.DeviceSerialNumber.value]['acquisition']

    nodemap.AcquisitionMode.value = 'Continuous'
    nodemap.TriggerSource.value = 'Software'
    nodemap.PixelFormat.value = 'BayerRG16'
    nodemap.TriggerActivation.value = 'RisingEdge'
    nodemap.TriggerDelayEnabled.value = True
    nodemap.TriggerMode.value = 'On'
    nodemap.TriggerOverlap.value = 'ReadOut'
    nodemap.ExposureMode.value = 'Timed'
    nodemap.ExposureAuto.value = 'Off'
    nodemap.GainAuto.value = 'Off'
    nodemap.BalanceWhiteAuto.value = 'Off'
    nodemap.pgrExposureCompensationAuto.value = 'Off'

    nodemap.TriggerDelay.value = config['trigger_delay']
    nodemap.Gain.value = config['gain']
    nodemap.ExposureTime.value = config['exposure_time']

    nodemap.BalanceRatioSelector.value = 'Blue'
    nodemap.BalanceRatio.value = config['balance_ratio_blue']
    nodemap.BalanceRatioSelector.value = 'Red'
    nodemap.BalanceRatio.value = config['balance_ratio_red']

    nodemap.Width.value = config['width']
    nodemap.Height.value = config['height']
    nodemap.OffsetX.value = config['offset_x']
    nodemap.OffsetY.value = config['offset_y']

    return image_acquirer


def single_capture_screen(camera):
    """
    :param camera: an image acquirer object from harvesters
    :return:
    """

    sg.theme('DefaultNoMoreNagging')
    layout = [[sg.Text('Save Location', font='Arial 15'), sg.InputText('/shared/calibration_images',
                                                                       key='-savedir-', font='Arial 15'),
               sg.FolderBrowse(target='-savedir-', font='Arial 15')],
              [sg.Button('Capture Image', size=(40, 3), font='Arial 15', button_color=(None, 'red'))],
              [sg.Image(filename='', key='-cam-', size=(375, 550))],
              [sg.Exit(size=(50, 3), font='Arial12')]]
    window = sg.Window('BELT Image Acquisition', layout, element_justification='center', resizable=True).Finalize()

    while True:
        # no waiting so cameras can stream images by requesting the image
        event, values = window.read(timeout=20, timeout_key='-timeout-')
        if event in (None, 'Exit'):
            break
        camera.remote_device.node_map.TriggerSoftware.execute()
        with camera.fetch_buffer() as buf:
            comp = buf.payload.components[0]
            img = np.copy(comp.data.reshape(comp.height, comp.width))
        disp_img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2BGR_EA)
        disp_img_bytes = cv2.imencode('.png', cv2.resize(disp_img, dsize=(0,0), fx=0.3, fy=0.3))[1].tobytes()
        window['-cam-'].update(data=disp_img_bytes)
        #todo include cropping from camsettings_dict[SN]['calibration']

        if event == 'Capture Image':
            popup_input = sg.PopupGetText('Captured Square {letter}{number}')
            img = cv2.cvtColor(img, cv2.COLOR_BAYER_BG2RGB_EA)  # to RGB for numpy & matplotlib
            img = (np.power(img / np.iinfo(img.dtype).max, 1/2.2) * (2**16)-1).astype(np.uint16)
            np.save(os.path.join(values['-savedir-'],
                                 '{}-{}.npy'.format(popup_input, camera.device.node_map.DeviceSerialNumber.value)), img)


def main():
    """
    main level, starts the harvester program and connects to a camera specified
    :return:
    """
    h = Harvester()
    h.add_cti_file('/opt/mvIMPACT_Acquire/lib/x86_64/mvGenTLProducer.cti')
    h.update_device_info_list()
    avail_cams = [cam.serial_number for cam in h.device_info_list]
    _, selected_cam = sg.Window('Pick a Camera', layout=[[sg.Listbox(values=avail_cams, size=(None,len(avail_cams)), enable_events=True, select_mode=sg.LISTBOX_SELECT_MODE_SINGLE )]])\
        .read(close=True)
    ia = h.create_image_acquirer(serial_number=selected_cam[0][0])

    ia = configure_device(ia)
    ia.start_acquisition()
    single_capture_screen(ia)


if __name__ == '__main__':
    main()