#  File: generateFiguresFromReport.py
#  Description: Holds functions that make plots to describe some a calibration report
#               Useful to see what colours are difficult for the calibration to correct to
#               and the relative magnitudes of the differences between prediction and target per channel
#
#  Author: Keith Halcro
#  Copyright: University of Saskatchewan, 2020
#

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from skimage.color import deltaE_ciede2000, lab2rgb


def open_report(csv_filepath):
    """
    opens the report created by colourCalibration.py and populates some more columns
    """
    df = pd.read_csv(csv_filepath)
    df['diff_l*'] = df['pred_l*'] - df['target_l*']
    df['diff_a*'] = df['pred_a*'] - df['target_a*']
    df['diff_b*'] = df['pred_b*'] - df['target_b*']
    df['delta_e'] = deltaE_ciede2000(df[['pred_l*', 'pred_a*', 'pred_b*']].values,
                                     df[['target_l*', 'target_a*', 'target_b*']].values)
    return df


def _make_boxplot(dataframe, column, title=None):
    """
    convenience function to make a boxplot using pandas then clean it up a little
    :param dataframe: dataframe to parse
    :param column: column to extract from dataframe
    :param title: optional string to pass to set title, defaults to column name
    :return: return boxplot axes for further modification if desired
    """
    bp = dataframe[['square_id', column]].boxplot(by='square_id', widths=0.9, rot=90)
    bp.set_title(column if title is None else title)
    bp.get_figure().suptitle(None)
    return bp


def create_statistic_plots(dataframe):
    """
    creates a handful of plots to summarize the per-channel statistical information
    :param dataframe: dataframe holding the camera calibration report
    """
    bp_l = _make_boxplot(dataframe, 'diff_l*', title='Difference in Lightness (l*)\nPrediction relative to Target')
    bp_l.plot()
    bp_a = _make_boxplot(dataframe, 'diff_a*', title='Difference in Colour (a*)\nPrediction relative to Target')
    bp_a.plot()
    bp_b = _make_boxplot(dataframe, 'diff_b*', title='Difference in Colour (b*)\nPrediction relative to Target')
    bp_b.plot()
    _, pie_ax = plt.subplots()
    p_sums = dataframe[['diff_l*', 'diff_a*', 'diff_b*']].pow(2).sum()
    p_sums.name = ''
    p_sums.plot(kind='pie', autopct='%.1f%%', title='Sum of Squares of Per-channel Differences ', ax=pie_ax)
    _, hist_ax = plt.subplots()
    dataframe.groupby('square_id').mean()[['diff_l*', 'diff_a*', 'diff_b*']].plot(kind='hist', subplots=True, bins=100,
                                                                                  xlim=(-5, 5), ax=hist_ax,
                                                                                  title='Mean Difference Distributions')
    _, bar_ax = plt.subplots()
    dataframe.groupby('square_id').mean()[['diff_l*', 'diff_a*', 'diff_b*']].plot(kind='bar', stacked=True, width=0.85,
                                                                                  ax=bar_ax,
                                                                                  title='Stacked Mean Differences '
                                                                                        '(zero_centre)')
    plt.show()


def create_perception_plots(dataframe):
    """
    creates a handful of plots to demonstrate which predictions do not match their targets within the Just Noticeable
    Difference (JND) range of Delta E < 2.3
    :param dataframe: dataframe holding the camera calibration report
    """
    bp_e = _make_boxplot(dataframe, 'delta_e', title='Colour Distance $\Delta$E')
    bp_e.axhline(2.3, color='r', label='Just Noticeable Difference')
    plt.legend()
    _, bar_ax = plt.subplots()
    dataframe.loc[dataframe['delta_e'] > 2.3]['square_id'].value_counts().plot(kind='bar', ax=bar_ax,
                                                                               title='Number of Squares with '
                                                                                     '$\Delta$E > 2.3 '
                                                                                     '(Just Noticeable Difference)')

    over_jnd = dataframe.loc[dataframe['delta_e'] > 2.3]
    plt.figure()
    g = over_jnd.groupby('square_id')
    plt.imshow(np.stack((lab2rgb(
        np.expand_dims(g[['target_l*', 'target_a*', 'target_b*']].mean().values, 1)).squeeze(),
                         g[['pred_r', 'pred_g', 'pred_b']].mean().values)))
    plt.gca().set_yticks([0, 1])
    plt.gca().set_yticklabels(['Target', 'Predicted'])
    plt.gca().set_xticks(np.arange(g.ngroups))
    plt.gca().set_xticklabels(list(g.groups))
    plt.title('Mean Square of predictions with $\Delta$E > 2.3\nTop row Target, Bottom row Predicted')
    plt.show()

