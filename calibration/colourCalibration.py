#  File: colourCalibration.py
#  Description: Takes full size images acquired with singleCapture.py and trains colour calibration neural networks on a
#  200x200 pixel crop using the offsets provided in camsettings.yaml
#
#  Author: Keith Halcro
#  Copyright: University of Saskatchewan, 2020
#

import glob
import sys
import os

import numpy as np
import pandas as pd
import joblib
import yaml

from skimage.color import lab2rgb
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split


class CalibrationImage:
    def __init__(self, image, square_id, camera_sn):
        self.image = image
        self.square_id = square_id
        self.camera_sn = camera_sn
        self.lab = None
        self.samples_per_image = 40
        self.medians = np.median(self.image.reshape(-1, self.samples_per_image, 3), axis=0)

    def set_lab(self, lab_input):
        self.lab = lab_input


def _open_lab_txt():
    """
    This lab file is *ONLY* available to be used for non-commercial products!!
    By default the lab file is included with BELT, but to respect copyright, it is not included in the git
    https://xritephoto.com/ph_product_overview.aspx?ID=938&Action=Support&SupportID=5158
    :return: A numpy array with shape (x, 1, 3)
    """
    try:
        calibration_table = pd.read_csv('digital_colorchecker_sg_l_a_b.txt', skiprows=(141, 142),
                                        index_col=0, sep='\t')
        return calibration_table
    except FileNotFoundError:
        print('Colorchecker file not found in default directory {}'.format(sys.path[0]))


def _collect_calibration_image_paths(selected_dir=None):
    """
    opens all calibration images for the specified camera
    expects filenames in the format {letter}{number}-{serial_number}.png
    :param selected_dir: an input directory
    :return: a list of image filepaths
    """
    if selected_dir is None:
        import PySimpleGUI as sg
        selected_dir = sg.PopupGetFolder('Select The Folder of ColorChecker Images (.npy)')
    image_paths = glob.glob(os.path.join(selected_dir, '*.npy'))
    return image_paths


def _open_camsettings(path=None):
    """
    opens the camera settings yaml file as a dictionary
    """
    if path is None:
        try:
            with open('./camsettings.yaml') as f:
                settings = yaml.safe_load(f)
        except FileNotFoundError:
            import PySimpleGUI as sg
            gui_supplied_path = sg.PopupGetFile('Select the camsettings.yaml file')
            with open(gui_supplied_path) as f:
                settings = yaml.safe_load(f)
    else:
        with open(path) as f:
            settings = yaml.safe_load(f)

    return settings


def create_calibration_image(npy_filename, settings_dict):
    """
    Initializes a CalibrationImage object and populates all of the attributes
    :param npy_filename: The string path to a npy file (RGB 16 bit colour image of w/ colour square in it
    :param settings_dict: a dictionary that contains per-camera information about the cropping locations
    :return: a CalibrationImage object
    """
    img = np.load(npy_filename)
    img = (img / np.iinfo(img.dtype).max).astype(np.float16)
    square_id, camera_sn = os.path.splitext(os.path.basename(npy_filename))[0].split('-')
    h = settings_dict[camera_sn]['calibration']['calib_height_offset']
    w = settings_dict[camera_sn]['calibration']['calib_width_offset']
    small_img = img[h:h+200, w:w+200]
    image_obj = CalibrationImage(small_img, square_id, camera_sn)

    return image_obj


def populate_calibration_image(calib_image_obj, calibration_table):
    """
    Takes the initialized calibration image object and adds the target Lab values and subsamples the image to prime the
    object to be used as a machine learning data source.
    :param calib_image_obj: A calibrationImage object that has been initialized
    :param calibration_table: pandas dataframe of the colorchecker L_a_b values
    :return: nothing, modifies object in place
    """
    calib_image_obj.set_lab((calibration_table.loc[calib_image_obj.square_id, :]).values.reshape(1, 1, 3))


def train_regressor(calib_image_list):
    """
    Extracts data from calibratedImage objects and trains the MLPR
    :param calib_image_list: A list of all calibratedImage objects created in this session
    :return: A variable length list of trained MLPR objects, creates one per unique camera serial number
    """
    mlpr_list = []
    unique_serial_numbers = sorted(set([ci.camera_sn for ci in calib_image_list]))
    samples_per_image = calib_image_list[0].samples_per_image
    for sn in unique_serial_numbers:
        cam_specific_calib_image_list = [ci for ci in calib_image_list if ci.camera_sn == sn]
        num_of_images = len(cam_specific_calib_image_list)
        medians = np.empty((num_of_images*samples_per_image, 3), dtype=np.float16)
        targets = np.empty((num_of_images*samples_per_image, 3), dtype=np.float16)
        ids = np.empty((num_of_images*samples_per_image, 1), dtype='U3')
        for i, ci in enumerate(cam_specific_calib_image_list):
            medians[i*samples_per_image:(i+1)*samples_per_image, :] = ci.medians
            targets[i*samples_per_image:(i+1)*samples_per_image, :] = np.repeat(ci.lab, samples_per_image, axis=1)
            ids[i*samples_per_image:(i+1)*samples_per_image, :] = ci.square_id
        train_x, test_x, train_y, test_y = train_test_split(medians, targets, stratify=ids)
        mlpr = MLPRegressor(max_iter=100000, hidden_layer_sizes=(50,), solver='adam', activation='tanh')
        mlpr.fit(train_x, train_y)
        mlpr_list.append(mlpr)
        print('**MLPR STATS FOR CAMERA {}'.format(sn))
        print('\tBuilt in scoring call: {:.4f}'.format(mlpr.score(test_x, test_y)))
        df = pd.DataFrame()
        df = pd.concat((df, pd.DataFrame(ids, columns=['square_id'])), axis='columns')
        df = pd.concat((df, pd.DataFrame(medians, columns=['meas_r', 'meas_g', 'meas_b'])), axis='columns')
        pred_lab = mlpr.predict(medians)
        pred_rgb = lab2rgb(np.expand_dims(pred_lab, 0)).squeeze()
        df = pd.concat((df, pd.DataFrame(pred_rgb, columns=['pred_r', 'pred_g', 'pred_b'])), axis='columns')
        df = pd.concat((df, pd.DataFrame(pred_lab, columns=['pred_l*', 'pred_a*', 'pred_b*'])), axis='columns')
        df = pd.concat((df, pd.DataFrame(targets, columns=['target_l*', 'target_a*', 'target_b*'])), axis='columns')

        df.to_csv('{}_report.csv'.format(sn), index=False)
        print('\tMean absolute difference : {:.3f} s.d. {:.3f}'
              .format((np.abs(lab2rgb(np.expand_dims(mlpr.predict(medians), 0)) -
                              lab2rgb(np.expand_dims(targets, 0)))).mean(),
                      (np.abs(lab2rgb(np.expand_dims(mlpr.predict(medians), 0)) -
                              lab2rgb(np.expand_dims(targets, 0)))).std()))
    return mlpr_list, unique_serial_numbers


if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--input', help='Path to input directory', required=False)
    ap.add_argument('-s', '--settings', help='Path to camsettings.yaml', required=False)
    args = ap.parse_args()
    filenames = _collect_calibration_image_paths(args.input)
    settings_dict = _open_camsettings(args.settings)
    calibration_table = _open_lab_txt()
    calibration_image_objs = [create_calibration_image(filename, settings_dict) for filename in filenames]
    [populate_calibration_image(ci, calibration_table) for ci in calibration_image_objs]
    mlprs, serial_numbers = train_regressor(calibration_image_objs)
    for mlpr, sn in zip(mlprs, serial_numbers):
        joblib.dump(mlpr, 'mlpr_{}.joblib'.format(sn))



