#  File: removeNumpyBinaryImages.py
#  Description: remove belt images that do not have an associated thumbnail
#  Useful if you manually remove thumbnails while reviewing
#
#  Author: Keith Halcro
#  Copyright: University of Saskatchewan, 2020
#

from glob import glob
import os
import argparse


def get_npy_files(base_directory):
    """
    make a list of every file ending with .npy
    :param base_directory: string path of the base directory to recursively search
    :return:
    """
    return glob(os.path.join(base_directory, '**', '*.npy'), recursive=True)


def get_jpg_files(base_directory):
    """
    make a list of every file ending with .jpg
    :param base_directory: string path of the base directory to recursively search
    :return:
    """
    return glob(os.path.join(base_directory, '**', '*.jpg'), recursive=True)


def compare_npy_to_jpg(npy_filepath, jpg_list):
    """
    rearranges elements of the npy filepath and checks against the jpg list
    if no match, the npy file is removed
    :param npy_filepath: string path to a npy file
    :param jpg_list: list of existing jpg files
    :return:
    """
    compare_name = os.path.join(os.path.dirname(npy_filepath), 'thumbnails',
                                os.path.splitext(os.path.basename(npy_filepath))[0] + '.jpg')
    if compare_name not in jpg_list:
        os.remove(npy_filepath)


def remove_numpy_binary_images(selected_dir):
    """
    parses through the passed directory to remove all npy files that do not have an associated jpg
    :param selected_dir: directory to recursively search
    :return:
    """
    npy_list = get_npy_files(selected_dir)
    jpg_list = get_jpg_files(selected_dir)

    for npy_file in npy_list:
        compare_npy_to_jpg(npy_file, jpg_list)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--input', help='Path to a directory to recursively search', default='/shared/DATA')
    args = ap.parse_args()

    remove_numpy_binary_images(args.input)
