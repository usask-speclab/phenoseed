"""
    File: phenoSEED.py
    Description:
        main file for phenoSEED, designed to work in conjunction with the image acquisition system BELT
        1) loads preprocessed files, falling back to loading images
        2) applies preprocessing if necessary
        2a) colour calibration to D65 illuminant and transformation to CIELab colour space
        2b) segmentation of the seed - create a one bit-depth mask for front view and a side view
        2c) crop the images to remove extraneous information
        3) passes images to main level processing
        3a) extract descriptive statistics of each L*a*b* colour channel
        3b) profile the shape and size of the view masks
        3c) cluster the seed coat colours into two groups using a Gaussian Mixture Model and report the cluster
        proportions, centres and distance
        4) append all main level processing information together and save all information line-by-line into a .csv file
    Author: Keith Halcro (keithhalcro@mail.usask.ca)
    Copyright: University of Saskatchewan, 2019
"""

import argparse
import glob
import multiprocessing as mp
import os
from datetime import datetime, timedelta
from itertools import repeat
import logging
# silence warning about using boolean array for single label
import warnings
warnings.filterwarnings('ignore', message='Only one label was provided to')

import yaml
import joblib
import numpy as np
import pandas as pd
from natsort import natsort_keygen

from scipy.ndimage.morphology import binary_fill_holes
from scipy.special import ellipkinc, ellipeinc
from skimage import filters, measure, morphology, transform, color, io, segmentation
from sklearn import mixture, model_selection


class SeedImage:
    """
    seed image object, contains methods to load an image from the filepath, segment and analyze it, then write the
    results to a file
    """

    def __init__(self, filepath):
        """
        :param file: path to a .npy file
        """
        # initialize expected attributes so pycharm doesn't have to warn me about it
        self.front_lab = np.array([])
        self.side_lab = np.array([])
        self.front_mask = np.array([])
        self.side_mask = np.array([])
        self.height = np.array([])
        self.height_scaling_position = np.array([])
        self.valid = True  # flag to keep applying analysis and eventually write this data
        # load the image
        self.filepath = filepath
        if '.npy' in self.filepath:
            self.image = np.load(self.filepath)
            self.sn = self.filepath.split(os.sep)[-1].split('_')[0]
        elif '.png' in self.filepath:
            self.image = io.imread(self.filepath)
            self.sn = self.filepath.split(os.sep)[-1].split('-')[0]
            if not all(e.isdigit() for e in self.sn):
                self.sn = self.filepath.split(os.sep)[-1].split('_')[0]
        self.image = self.image / np.iinfo(self.image.dtype).max

        # get parameters for processing
        self.parameters = None
        self.open_settings()
        self.mlpr = joblib.load(os.path.join(os.path.dirname(__file__), 'calibration', f'mlpr_{self.sn}.joblib'))
        # make a series object - fill it with analysis later
        self.ser = pd.Series()
        # call standard methods to populate the lab and mask attributes
        self.calibrate_and_segment()
        outer_ring_mask = np.ones_like(self.front_mask).astype(np.bool)
        outer_ring_mask[10:-10, 10:-10] = False
        if np.any(self.front_mask[outer_ring_mask]):
            logging.warning(f'Mask for {self.filepath} touches outer edge of cropped image and is discarded')
            self.valid = False
        if self.front_mask.sum() < 100 or self.side_mask.sum() < 100:
            logging.warning(f'Mask is too small for {self.filepath}')
            self.valid = False

        if self.valid:
            front_region = measure.regionprops(measure.label(self.front_mask))
            side_region = measure.regionprops(measure.label(self.side_mask))

            # i know there's only one region.. so I can call region[0]
            # (front_min_row, front_min_col, front_max_row, front_max_col) = front_region[0].bbox
            # (side_min_row, side_min_col, side_max_row, side_max_col) = side_region[0].bbox

            self.front_lab = self.front_lab[front_region[0].slice]
            self.front_mask = self.front_mask[front_region[0].slice]
            self.side_mask = self.side_mask[side_region[0].slice]
            self.side_lab = self.side_lab[side_region[0].slice]
            # this next bit requires some explanation. we want to scale the reflection in the prism correctly so we need
            # to extract where the seed is in the x-direction. We look at the bounding rows and take the medians of the
            # mask pixels there then median the two 'endpoints' to compromise between the two. No assumptions of
            # roundness or eccentricity are made
            self.height_scaling_position = np.median((np.median(np.nonzero(self.front_mask[0])),
                                                     np.median(np.nonzero(self.front_mask[-1]))))

    def calibrate_and_segment(self):
        """
        crop the image, pass it through the mlpr and generate a mask for the seed areas - front and side views
        :return: the cropped front view in lab colourspace, the front mask and the side mask
        """
        # load in cropping coordinates and the mlpr
        front_crop = self.parameters['front_crop_coords']
        side_crop = self.parameters['side_crop_coords']
        front_rgb = self.image[front_crop[0]:front_crop[1], front_crop[2]:front_crop[3], :]  # crop
        side_rgb = self.image[side_crop[0]:side_crop[1], side_crop[2]:side_crop[3], :]
        front_shape = front_rgb.shape  # store image dimensions
        side_shape = side_rgb.shape
        # mlpr is trained to accept an array of length three (r, g, b) and return a an array of length three (l, a, b)
        self.front_lab = self.mlpr.predict(front_rgb.reshape(-1, 3)).reshape(front_shape)
        self.side_lab = self.mlpr.predict(side_rgb.reshape(-1, 3)).reshape(side_shape)
        # make masks
        self.create_masks()

    def create_masks(self, front=True):
        """
        creates a mask encompassing the seed by thresholding a linear combination of l,a,b channels
        :param front: boolean True if the view is a front, otherwise is a side view.
        """
        if front:
            lab = np.copy(self.front_lab)
            # perceptual distance from the nominal background (bright white)
            de = color.deltaE_ciede2000(lab, (95, 0, 0))
        else:
            lab = np.copy(self.side_lab)
            # chromaticity difference from the median chromaticity of the front view segmented
            de = np.abs(color.lab2lch(lab)[..., 1] - np.median(color.lab2lch(self.front_lab)[self.front_mask][..., 1], axis=-1))
        thresh = filters.threshold_otsu(de)
        # we want difference from background for front - and similarity for side mask
        if front:
            mask = de > thresh
        else:
            mask = de < thresh
        mask = binary_fill_holes(mask)
        label = morphology.label(mask)  # label the mask image (assign unique number to each region
        vals, counts = np.unique(label[label != 0], return_counts=True)  # count number of pixels in each label
        sorted_label_idxes = vals[np.argsort(counts)]  # sort to find the id of largest region
        largest_object_mask = np.zeros_like(mask).astype(np.bool)  # create array of zeros
        if sorted_label_idxes.size != 0:
            largest_object_mask[label == sorted_label_idxes[-1]] = True  # create boolean mask of largest region

        # assign the mask to the correct attribute
        if front:
            self.front_mask = largest_object_mask
            # if the mask is empty, stop processing
            if self.front_mask.sum() == 0:
                self.valid = False
            else:
                self.create_masks(not front)
        else:
            self.side_mask = largest_object_mask
            # assume the maximum vertical line in each mask is close (height scaling - pixel counts may not be perfect)
            # generously, should be within 15% of each other, if not... not sure which is untrue

            front_props = measure.regionprops(measure.label(self.front_mask))
            side_props = measure.regionprops(measure.label(self.side_mask))
            front_vert_max = np.abs(np.subtract(*front_props[0].bbox[::2]))
            side_vert_max = np.abs(np.subtract(*side_props[0].bbox[::2]))
            if not (1.15 >= side_vert_max / front_vert_max >= 0.85):
                logging.info(f'Segmentation mismatch: max vertical line in reflection does not agree '
                             f'with front view for\n{self.file}')
                self.height = np.array(0)

    def shape_descriptors(self):
        """
        extract morphological information including size and shape descriptors from images
        """
        if self.height.size == 0:
            self.height_from_side()
        region = measure.regionprops(measure.label(self.front_mask))
        front_height_scaling = self.parameters['front_height_scaling']

        # i know there is only one region so I can call region[0]
        data_dict = {}
        data_dict['height[mm]'] = self.height
        data_dict['area[px]'] = region[0].area
        data_dict['area[mm^2]'] = region[0].area * front_height_scaling * front_height_scaling
        data_dict['perimeter[mm]'] = region[0].perimeter * front_height_scaling
        data_dict['major_axis_length[mm]'] = region[0].major_axis_length * front_height_scaling
        data_dict['minor_axis_length[mm]'] = region[0].minor_axis_length * front_height_scaling
        data_dict['equivalent_diameter[mm]'] = region[0].equivalent_diameter * front_height_scaling

        # shape statistics built on extracted measurements
        data_dict['roundness(4*area/(pi*major_axis^2)'] = \
            4 * data_dict['area[mm^2]'] / (np.pi * np.square(data_dict['major_axis_length[mm]']))
        data_dict['circularity(4*pi*area/(perimeter^2))'] = \
            4 * np.pi * data_dict['area[mm^2]'] / np.square(data_dict['perimeter[mm]'])
        data_dict['volume[mm^3]'] = \
            np.pi * data_dict['major_axis_length[mm]'] * data_dict['minor_axis_length[mm]'] * self.height / 6
        # calculate sphericity...need to ensure strictly increasing order of semi-axes a >= b >= c
        a, b, c = np.sort(np.array((data_dict['major_axis_length[mm]'] / 2, data_dict['minor_axis_length[mm]'] / 2,
                                    self.height / 2)))[::-1]
        # https://en.wikipedia.org/wiki/Ellipsoid - See Volume and Surface Area
        phi = np.arccos(c / a)
        k = np.sqrt(np.square(a) * (np.square(b) - np.square(c)) / (np.square(b) * (np.square(a) - np.square(c))))
        temp = ellipeinc(phi, k) * np.square(np.sin(phi)) + ellipkinc(phi, k) * np.square(np.cos(phi))
        data_dict['surface_area[mm^2]'] = 2 * np.pi * (np.square(c) + a * b * temp / np.sin(phi))
        data_dict['sphericity(pi^(1/3)*(6*volume)^(2.3))/(surface_area)'] = \
            np.power(np.pi, 1 / 3) \
            * np.power(6 * data_dict['volume[mm^3]'], 2 / 3) \
            / data_dict['surface_area[mm^2]']

        self.ser = pd.concat([self.ser, pd.Series(data_dict)])

    def height_from_side(self):
        """
        extract height measurement from the side view. this is by far the ugliest function
        """
        # scale based on seed position relative to the reflective prism
        side_height_scaling = self.parameters['side_height_scaling_intercept'] + \
                              (self.parameters['side_height_scaling_slope'] * self.height_scaling_position)
        # the idea here is to find the furthest points on the seed and draw a line between to 'split' the seed into
        # a half then look at the curve produced
        side_view_max_height = self.side_mask.shape[-1]  # number of columns
        edge_position = np.argmax(self.side_mask, axis=1)  # scans each row and returns the index of the first True value
        top_edge = side_view_max_height - edge_position  # changes origin to the 'bottom' of the prism
        # endpoints of seed
        x_1 = 0
        x_2 = top_edge.size - 1
        y_1 = top_edge[x_1]
        y_2 = top_edge[x_2]
        midline_angle = np.arctan2(y_2 - y_1, x_2 - x_1) * 180 / np.pi
        rotated_mask = transform.rotate(self.side_mask, midline_angle, center=(side_view_max_height - y_1, x_1),
                                        resize=True)  # rotate so that the midline is vertical

        nonzero_rows = np.nonzero(rotated_mask.sum(axis=1))[0]  # now have padded space from the rotation function
        # find position of top edge after rotating the mask
        rotated_edge_position = np.argmax(rotated_mask[nonzero_rows[0]:nonzero_rows[-1], :], axis=1)
        # the mask was rotated about a point on the top row (x_1 = 0), so the first point of the rotated_edge is on
        # the midline by definition and we're referencing from that to get the curve of the upper half
        half_heights = rotated_edge_position[0] - rotated_edge_position
        half_max_height = half_heights.max() * side_height_scaling  # apply scaling
        self.height = 2 * half_max_height  # save as an attribute to enable shape_descriptors()

    def colour_statistics(self):
        """
        extract colour stats from the images
        """
        # extract channels and apply mask
        l = self.front_lab[..., 0][self.front_mask]
        a = self.front_lab[..., 1][self.front_mask]
        b = self.front_lab[..., 2][self.front_mask]
        lab_list = ('l', 'a', 'b')  # make a list of the identifiers (replace this with f-string debugging in 3.8)
        data_dict = {}  # prep data dictionary
        for i, component in enumerate((l, a, b)):
            data_dict[f'{lab_list[i]}_mean'] = component.mean()
            data_dict[f'{lab_list[i]}_std'] = component.std()
            data_dict[f'{lab_list[i]}_min'] = component.min()
            data_dict[f'{lab_list[i]}_25'] = np.percentile(component, 25)
            data_dict[f'{lab_list[i]}_50'] = np.percentile(component, 50)
            data_dict[f'{lab_list[i]}_75'] = np.percentile(component, 75)
            data_dict[f'{lab_list[i]}_max'] = component.max()

        self.ser = pd.concat([self.ser, pd.Series(data_dict)])

    def colour_clustering(self, train_amount=0.1):
        """
        cluster the lab image into two groups using a Gaussian Mixture Model. Select 10% of the data to train with
        """
        # initialize the model with two clusters - leave most parameters as default
        gm = mixture.GaussianMixture(n_components=2)
        train_data, _ = model_selection.train_test_split(self.front_lab[self.front_mask], train_size=train_amount)
        gm.fit(train_data)  # fit the model
        gm_predictions = gm.predict(self.front_lab[self.front_mask])  # make predictions
        _, counts = np.unique(gm_predictions, return_counts=True)
        proportions = counts / self.front_mask.sum()
        data_dict = {}
        data_dict['cluster_distance'] = color.deltaE_ciede2000(gm.means_[0], gm.means_[1])  # cluster centre distance
        # order clusters based on lightness of the centre
        dark_cluster_index = np.argmin(gm.means_[:, 0])
        light_cluster_index = np.argmax(gm.means_[:, 0])
        data_dict['dark_cluster_l'] = gm.means_[dark_cluster_index][0]
        data_dict['dark_cluster_a'] = gm.means_[dark_cluster_index][1]
        data_dict['dark_cluster_b'] = gm.means_[dark_cluster_index][2]
        data_dict['dark_cluster_proportion'] = proportions[dark_cluster_index]
        data_dict['light_cluster_l'] = gm.means_[light_cluster_index][0]
        data_dict['light_cluster_a'] = gm.means_[light_cluster_index][1]
        data_dict['light_cluster_b'] = gm.means_[light_cluster_index][2]
        data_dict['light_cluster_proportion'] = proportions[light_cluster_index]

        self.ser = pd.concat([self.ser, pd.Series(data_dict)])

    def make_rgb(self, view='front'):
        """
        Make an RGB image of the front or side lab

        :param view: string of 'front' or 'side'
        :return: uint8 RGB image of front/side_lab after calibration
        """
        if view == 'front' or view == 'side':
            return (color.lab2rgb(self.front_lab if view == 'front' else self.side_lab) * 255).astype(np.uint8)
        else:
            print('Specify either \'side\' or \'front\'')
            return

    def open_settings(self, settings_path=None):
        """
        populate global variables with the settings dictionary
        :param settings_path: a string path to the camsettings.yaml file .. defaults to ./camsettings.yaml
        """
        if settings_path is None:
            settings_path = os.path.join(os.path.dirname(__file__), 'camsettings.yaml')
        try:
            with open(settings_path) as f:
                yaml_dict = yaml.safe_load(f)
        except FileNotFoundError:
            import PySimpleGUI as sg
            gui_settings_path = sg.PopupGetFile('Select the camsettings.yaml file', file_types=(('YAML', '*.yaml'),))
            with open(gui_settings_path) as f:
                yaml_dict = yaml.safe_load(f)

        self.parameters = yaml_dict[self.sn]['processing']


class SeedImagePreProcessed(SeedImage):
    """
    subclass to change init method - front lab, front_mask, side_mask and height_scaling_position are already calculated
    """
    def __init__(self, file):
        """
        :param file: string path to a .npz file containing 'front_lab', 'front_mask', 'side_mask', and
        'height_scaling_position'
        """
        self.file = file
        npzfile = np.load(file)
        self.front_lab = npzfile['front_lab']
        self.front_mask = npzfile['front_mask']
        self.side_lab = npzfile['side_lab']
        self.side_mask = npzfile['side_mask']
        self.height_scaling_position = npzfile['height_scaling_position']
        self.height = None
        self.valid = True
        self.sn = file.split(os.sep)[-1].split('_')[0]

        self.ser = pd.Series()


def run_one_file(filepath, input_dir, output_dir):
    """
    creates a SeedImage object for input file, runs processing and appends the data to a csv
    will apply preprocessing and save out the intermediate files if supplied a .npy file
    :param filepath: a string path to either a standalone .npy produced by BELT containing RGB image or a path to an
    .npz file containing already processed arrays of lab and mask information
    :param input_dir: string path to the user supplied base directory - used to identify metadata in file structure
    :param output_dir: string path to the output directory
    """
    # create metadata
    metadata_dict = {}
    metadata_dict['file_path'] = filepath  # the full path
    # split all directories between input and basename - want to be able to easily group these in excel
    metadata_directories = [meta_dir for meta_dir in os.path.split(filepath)[0].split(os.sep)
                            if meta_dir not in input_dir.split(os.sep) and meta_dir != '']
    for i, metadata_dir in enumerate(metadata_directories):
        metadata_dict[f'metadata_directory_{i}'] = metadata_dir

    # initialize based on input file type
    if '.npz' in os.path.splitext(filepath)[-1]:
        si = SeedImagePreProcessed(filepath)
    elif '.npy' in os.path.splitext(filepath)[-1] or '.png' in os.path.splitext(filepath)[-1]:
        try:
            si = SeedImage(filepath)
        except ValueError as e:
            logging.error(f'Caught Error while opening {filepath}')
            logging.error(e)
            return
        except SyntaxError as e:  # only occurs with broken png (i.e. empty file)
            logging.error(f'Caught SyntaxError while opening {filepath}')
            logging.error(e)
            return
        metadata_dict['serial_number'] = si.sn
        metadata_dict['processing_date'] = datetime.today().strftime('%Y-%m-%d')
        os.makedirs(os.path.join(output_dir, *metadata_directories), exist_ok=True)
        np.savez_compressed(os.path.join(output_dir, *metadata_directories,
                                         os.path.splitext(os.path.basename(filepath))[0] + '.npz'),
                            front_lab=si.front_lab, front_mask=si.front_mask, side_lab=si.side_lab,
                            side_mask=si.side_mask,
                            height_scaling_position=si.height_scaling_position)  # save segmented calibrated results
    else:
        logging.error(f'File passed to run_one_file that does not have .npy or .npz extension!\n{filepath}')
        return
    # call the processing functions and push to output if object is valid (has a calibrated lab image and masks)
    if si.valid:
        si.shape_descriptors()
        si.colour_statistics()
        si.colour_clustering()
        si.ser = pd.concat([pd.Series(metadata_dict), si.ser])

        data = pd.DataFrame(si.ser).T
        # TODO pass data to a queue and make a separate process save things
        data_out_location = os.path.join(output_dir, os.path.basename(os.path.normpath(input_dir)) + '_unsorted_.csv')
        data.to_csv(data_out_location, index=False, mode='a', header=False if os.path.isfile(data_out_location) else True)
        os.makedirs(os.path.join(output_dir, *metadata_directories, 'thumbnails'), exist_ok=True)
        si.save_jpg(os.path.join(output_dir, *metadata_directories, 'thumbnails',
                                 os.path.splitext(os.path.basename(filepath))[0] + '.jpg'))
    else:
        logging.debug(f'{filepath} was not processed')


if __name__ == '__main__':
    ap = argparse.ArgumentParser()  # construct parser
    ap.add_argument('-i', '--input', type=str, required=False,
                    help='path to input data - will be searched recursively')
    ap.add_argument('-s', '--settings', type=str, required=False, default='./camsettings.yaml',
                    help='path to camsettings.yaml file shared by phenoSEED and BELT')
    ap.add_argument('-o', '--output', type=str, required=False,
                    help='optional - path to write processed data and csv summary to, defaults to input')
    ap.add_argument('--log', help='the desired log level to be displayed', default='INFO',
                    choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
    args = ap.parse_args()

    debug_level = getattr(logging, args.log, None)
    logging.basicConfig(level=debug_level)

    # input setup
    if args.input is None:
        import PySimpleGUI as sg
        input_path = sg.PopupGetFolder('Select an input folder for phenoSEED')
    else:
        input_path = args.input

    # output setup
    if args.output is None:
        output_path = input_path
    else:
        output_path = args.output
        if not os.path.exists(output_path):
            os.makedirs(output_path)

    logging.info('searching for preprocessed .npz files')
    files = glob.glob(os.path.join(input_path, '**', '*.npz'), recursive=True)
    if len(files) == 0:
        logging.info('falling back to searching for .npy files')
        files = glob.glob(os.path.join(input_path, '**', '*.npy'), recursive=True)
        if len(files) == 0:
            logging.info('falling back to searching for .png files')
            files = glob.glob(os.path.join(input_path, '**', '*.png'), recursive=True)
            if len(files) == 0:
                logging.error(f'there are no npz, npy or png files found in supplied directory\n\t{args.input}')
                exit()
    files.sort()

    # report on what was input and expected outputs
    logging.info(f'input:\t\t\t{input_path}')
    logging.info(f'output:\t\t\t{output_path}')
    logging.info(f'files found:\t\t\t{len(files)}')
    # report on timing
    start_time = datetime.now()

    # create a pool of workers
    with mp.Pool() as pool:
        results = pool.starmap(run_one_file, zip(files, repeat(input_path), repeat(output_path)))

    # report on completion
    end_time = datetime.now()
    observed_seconds_per_file = (((end_time - start_time) / len(files)).seconds +
                                 ((end_time - start_time) / len(files)).microseconds / 1000000)
    logging.info(f'completion time\t\t{end_time.strftime("%b-%d %H:%M")}')
    logging.info(f'actual seconds per file:\t{observed_seconds_per_file}')

    logging.info('resorting csv')  # pool does not preserve file order
    unsorted_csv_locations = glob.glob(os.path.join(output_path, '**', '*_unsorted_.csv'), recursive=True)
    for csv_location in unsorted_csv_locations:
        try:
            df = pd.read_csv(csv_location)
            df.sort_values(by=['file_path'], inplace=True, key=natsort_keygen())
            df.to_csv(csv_location, index=False)
            csv_with_timestamp = '{0}{2}T{3}{1}'.format(*os.path.splitext(csv_location),
                                                        end_time.strftime('%y%m%d'),
                                                        end_time.strftime('%H%M%S')).replace('_unsorted_', '')
            os.rename(csv_location, csv_with_timestamp)
        except FileNotFoundError:
            logging.error(f'processing csv not found at {csv_location}')
            continue
        except KeyError:
            logging.warning('unable to sort csv by key "file_name"')
        continue
    logging.info('phenoSEED complete')
